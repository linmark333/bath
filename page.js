var animItem;

function insertAnimation() {
  var svgContainer = document.getElementById('svgContainer');
  var btn1 = document.getElementById("btn1");

  btn1.style.opacity = 0.5;
  btn1.disabled = true;
  var old_value = btn1.textContent;
  btn1.textContent = '準備中';

  animItem = lottie.loadAnimation({
    container: svgContainer,
    renderer: 'svg',
    path: 'All_with_dragon_x4.json', // the path to the animation json
    // loop: true,
    autoplay: false,
  });

  animItem.addEventListener('loaded_images', function() {
    console.log('data ready');
    btn1.style.opacity = 1;
    btn1.disabled = false;
    btn1.textContent = old_value;

  }, false);
}

function bathAlert() {
  window.alert("雙手合十（合掌）\n至誠恭敬稱念「南無本師釋迦牟尼佛」三遍");
}

function playAudio() {
  var x = document.getElementById("bgMusic");
  if ( x !== null && typeof x.play !== "undefined" ) {
    x.play();
  }
}

function showBtn2() {
  var btn1 = document.getElementById("btn1");
  var btn2 = document.getElementById("btn2");

  btn1.style.opacity = 0.5;
  btn1.disabled = true;

  setTimeout(() => {
    btn1.style.display = "none";
    btn2.style.display = "block";
  }, 15000);


}
function showBtn3() {
  var btn2 = document.getElementById("btn2");
  var btn3 = document.getElementById("btn3");

  btn2.style.opacity = 0.5;
  btn2.disabled = true;

  setTimeout(() => {
    btn2.style.display = "none";
    btn3.style.display = "block";
  }, 15000);
}

function repeatBtn() {
  var btn = document.getElementById("btnRepeat");

  btn.style.opacity = 0.5;
  btn.disabled = true;

  setTimeout(() => {
    btn.style.opacity = 1;
    btn.disabled = false;
  }, 12000);
}

function closeBtn3() {
  var btn3 = document.getElementById("btn3");
  var btnDonation = document.getElementById("btnDonation");
  var btnRepeat = document.getElementById("btnRepeat");

  btn3.disabled = true;
  btn3.style.opacity = 0.5;
  setTimeout(() => {
    btn3.style.display = "none";
    btnDonation.style.display = "inline-block";
    btnRepeat.style.display = "inline-block";

  }, 13000);
}
