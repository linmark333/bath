Buddha Bath Site, https://www.bathingbuddha.blisswisdom.us/

![](./screenshot.png)

### Dev & Testing
Start a simple python webserver to serve content.

```
# Python 2
python -m SimpleHTTPServer 8000

# Python 3
python -m http.server 8000
```
### Deploying Site
rsync -avz * us.blisswisdom.org:/var/www/bath/

### Animation
After Effect is used to create animation.  Bodymovin is a AE plugin that converts animation into SVG.  Lottie-web is a player that can control such animation.

https://github.com/airbnb/lottie-web

### Audio
Convert audio to low fidelity for file size.

Install community version of ffmpeg that has fdk-aac which can convert ALAC(loseless) to low bitrate AAC m4a.

```
brew uninstall ffmpeg
brew tap homebrew-ffmpeg/ffmpeg
# See possible options
brew options homebrew-ffmpeg/ffmpeg/ffmpe
# Install with fdk
brew install homebrew-ffmpeg/ffmpeg/ffmpeg --with-fdk-aac --HEAD

# To get smallest file with usable quality
ffmpeg -i input.wav -c:a libfdk_aac -profile:a aac_he_v2 -b:a 32k output.m4a

```

### Perf Test
Using siege to load test Linode single CPU 2Gig machine, handles about 165 concurrent users.

```
malin ~/dev/bath (master) $ siege -f urls.txt -c 200 -i -t 30s

{       "transactions":                         2501,
        "availability":                        97.09,
        "elapsed_time":                        29.39,
        "data_transferred":                   101.81,
        "response_time":                        1.93,
        "transaction_rate":                    85.10,
        "throughput":                           3.46,
        "concurrency":                        164.61,
        "successful_transactions":              2519,
        "failed_transactions":                    75,
        "longest_transaction":                 25.39,
        "shortest_transaction":                 0.07
}
```
